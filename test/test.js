var assert = require("assert"),
	should = require( 'chai' ).should();

describe('Batch Process', function(){
  describe('tinyPNGProcess', function(){
  	var tinyBatch = require('../');
  	//console.log(tinyBatch.tinyPNGProcess());
  	it('Should return false when called without arguments', function(){
  		assert.equal(false, tinyBatch.tinyPNGProcess());
  	});
  	it('should return an error if called without an apiKey', function(){
  		assert.equal(false, tinyBatch.tinyPNGProcess('test/', 'test/optimized'));
  	});
  	it('should return an error if called with an invalid apiKey', function(done){
  		this.timeout(15000);
  		return tinyBatch.tinyPNGProcess('test/', 'test/optimized/', '123456').should.be.rejected.and.notify(done);
  	});
  	/*
  	console.log(process.cwd());
  	
  	*/
  });
});