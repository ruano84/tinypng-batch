var fs = require('fs'),
	Q = require('Q'),
	request = require('request'),
	child = require('child_process'),
	Emitter = require('events').EventEmitter,
	emitter = new Emitter(),
	deferred,
	fileArray = [],
	currentItem = 0,
	isProcessing = false,
	inPath = '',
	outPath = '',
	apiKey = '';

/*var outputPromise = getInputPromise()
.then(function (input) {
}, function (reason) {
});*/

var processNext = function(file){
	var index, stat, user, output;
	user = new Buffer('api:' + apiKey).toString('base64');
	stat = fs.statSync(inPath + file);

	if(stat.isFile() && (file.indexOf('.png') != -1 || file.indexOf('.jpg') != -1)) {
		console.log('processing file #', currentItem, inPath + file);
		output = fs.createWriteStream(outPath + file);

		request.post({
			'url': 'https://api.tinypng.com/shrink',
			body: fs.readFileSync(inPath + file),
			headers: {
				'Authorization': 'Basic ' + user
			}
		}, function(_err, _result){
			var parsed, body;
			parsed = _result.toJSON();
			body = JSON.parse(parsed.body);
			
			if(_err){
				console.error(_err.toJSON());
				emitter.emit('ready');
				return _err;
			}
			if(body.error)
			{
				deferred.reject(new Error('there is an error'));
				//emitter.emit('ready');
				return body;
			}
			console.log('result: ', parsed.body);
			request(parsed.headers.location).pipe(output);
			// Process complete, we continue to the next file
			emitter.emit('ready');
		});
		console.log('end post? NOT');
	} else {
		// Wasn't a valid file, so we continue to the next file
		console.log('file ' + inPath + file + ' wastn allowed');
		emitter.emit('ready');
	}
};

var tinyPNGProcess = function(_inPath, _outPath, _apiKey, _callback) {
	deferred = Q.defer();
	inPath = _inPath;
	outPath = _outPath;
	apiKey = _apiKey;

	if(!fs.existsSync(inPath) || !fs.existsSync(outPath)){
		console.error('The input or output path invalid');
		return false;
	}

	if(typeof apiKey == 'undefined' || !apiKey) {
		return false;
	}

	if(typeof _callback == 'function') {
		callback = _callback;
	}

	fs.readdir(inPath, function(_err, _files){
		var index, file, stat, user, output;
		if(_err){
			console.error(_err);
			return _err;
		}

		fileArray = _files;
		
		emitter.on('ready', function(){
			if(currentItem < fileArray.length){
				// Next item in the array
				processNext(fileArray[currentItem++]);
			} else {
				// There arent more items
				console.log('return and end array');
				deferred.resolve(new Error());
			}
		});
		emitter.emit('ready');
	});
	return deferred.promise;
};

exports.tinyPNGProcess = tinyPNGProcess;

if(require.main === module){
	if(process.argv.length < 4)
	{
		console.error('tinypng-batch');
		console.error('v0.1.0\n');
		console.error('Usage: ');
		console.error('\ttinypng-batch inPath outPath apiKey');
	} else {
		tinyPNGProcess(process.argv[2], process.argv[3], process.argv[4]);
	}
}
